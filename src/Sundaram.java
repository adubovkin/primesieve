import java.util.Arrays;

import static java.lang.System.out;

/**
 * Sieve of Sundaram:
 * http://ru.wikipedia.org/wiki/%D0%A0%D0%B5%D1%88%D0%B5%D1%82%D0%BE_%D0%A1%D1%83%D0%BD%D0%B4%D0%B0%D1%80%D0%B0%D0%BC%D0%B0
 * - byte access
 */
public class Sundaram implements Sieve {
    public static final int BIT_SHIFT = 3;      // bit field shift
    public static final int BIT_MASK = 0x07;    // bit field mask

    private static byte[] bit_masks = new byte[8];
    private static byte[] neg_bit_masks = new byte[8];
    static {
        for (int i = 0; i < bit_masks.length; i++) {
            bit_masks[i] = (byte) (1 << i);
            neg_bit_masks[i] = (byte) ~bit_masks[i];
        }
    }

    @Override
    public int find(long N, boolean print) {
        byte[] data = new byte[(int) ((N >>> (BIT_SHIFT + 1)) + 1)];
        Arrays.fill(data, (byte) -1);
        int maxI = (int) ((Math.sqrt(N) - 1) / 2);
        int maxN = (int) (N >>> 1);
        for (int i = 1; i <= maxI; i++) {
            int test = 2 * i * (i + 1);
            int step = 2 * i + 1;
            while (test <= maxN) {
                int offs = test >>> BIT_SHIFT;
                int bit_offs = test & BIT_MASK;
                data[offs] &= neg_bit_masks[bit_offs];
                test += step;
            }
        }
        if (print)
            out.println("2");
        int count = 1;
        for (int index = 1; index <= maxN; index++) {
            int offs = index >>> BIT_SHIFT;
            int bit_offs = index & BIT_MASK;
            if ((data[offs] & bit_masks[bit_offs]) != 0) {
                count++;
                if (print)
                    out.println(index * 2 + 1);
            }
        }
        return count;
    }

    @Override
    public String getName() {
        return "Sundaram, byte access";
    }
}
