/**
 * User: alexd
 * Date: 4/1/13
 * Time: 11:27 AM
 */
public interface Sieve {
    public int find(long N, boolean print);
    public String getName();
}
