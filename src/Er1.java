import java.util.Arrays;

import static java.lang.System.out;

/**
 * Sieve of Eratosthenes:
 * - byte access
 * - bit field with 8 numbers per byte:
 *      numbers:   1  2  3  4  5  6  7  8
 *      bit field: 1  0  1  0  1  0  1  0
 */
public class Er1 implements Sieve {
    public static final int BIT_SHIFT = 3;      // bit field shift
    public static final int BIT_MASK = 0x07;    // bit field mask

    private static byte[] bit_masks = new byte[8];
    private static byte[] neg_bit_masks = new byte[8];
    static {
        for (int i = 0; i < bit_masks.length; i++) {
            bit_masks[i] = (byte) (1 << i);
            neg_bit_masks[i] = (byte) ~bit_masks[i];
        }
    }

    @Override
    public int find(long N, boolean print) {
        byte[] data = new byte[(int) ((N >>> BIT_SHIFT) + 1)];  // 8 numbers per byte
        Arrays.fill(data, (byte) 0xaa);    // binary 10101010
        int maxN = (int) Math.sqrt(N);
        for (int n = 3; n <= maxN; n += 2) {
            int offs = n >>> BIT_SHIFT;
            int bit_offs = n & BIT_MASK;
            if ((data[offs] & bit_masks[bit_offs]) != 0) {
                int n2 = n << 1;
                for (int k = n2 + n; k <= N; k += n2) {
                    offs = k >>> BIT_SHIFT;
                    bit_offs = k & BIT_MASK;
                    data[offs] &= neg_bit_masks[bit_offs];
                }
            }
        }
        if (print)
            out.println("2");
        int count = 1;
        for (int n = 3; n <= N; n += 2) {
            int offs = n >>> BIT_SHIFT;
            int bit_offs = n & BIT_MASK;
            if ((data[offs] & bit_masks[bit_offs]) != 0) {
                count++;
                if (print)
                    out.println(n);
            }
        }
        return count;
    }

    @Override
    public String getName() {
        return "8 num/byte, byte access";
    }
}
