import java.util.Arrays;

import static java.lang.System.out;

/**
 * Sieve of Eratosthenes:
 * - integer access (4 bytes)
 * - bit field with 16 numbers per byte (even numbers are stripped):
 *      numbers:   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
 *      bit field: 1     1     1     1     1     1     1     1
 */
public class Er3 implements Sieve {
    public static final int BIT_SHIFT = 5;      // bit field shift
    public static final int BIT_MASK = 0x1f;    // bit field mask

    private static int[] bit_masks = new int[32];
    private static int[] neg_bit_masks = new int[32];
    static {
        for (int i = 0; i < bit_masks.length; i++) {
            bit_masks[i] = 1 << i;
            neg_bit_masks[i] = ~bit_masks[i];
//            out.println(i + ") " + Integer.toBinaryString(bit_masks[i]) + "\t" + Integer.toBinaryString(neg_bit_masks[i]));
        }
    }

    @Override
    public int find(long N, boolean print) {
        int[] data = new int[(int) ((N >>> (BIT_SHIFT + 1)) + 1)];  // 16 numbers per byte
        Arrays.fill(data, -1);
        int maxN = (int) Math.sqrt(N);
        for (int n = 3; n <= maxN; n += 2) {
            int index = (n - 1) >>> 1;
            int offs = index >>> BIT_SHIFT;
            int bit_offs = index & BIT_MASK;
            if ((data[offs] & bit_masks[bit_offs]) != 0) {
                int n2 = n << 1;
                for (int k = n2 + n; k <= N; k += n2) {
                    index = (k - 1) >>> 1;
                    offs = index >>> BIT_SHIFT;
                    bit_offs = index & BIT_MASK;
                    data[offs] &= neg_bit_masks[bit_offs];
                }
            }
        }
        if (print)
            out.println("2");
        int count = 1;
        for (int n = 3; n <= N; n += 2) {
            int index = (n - 1) >>> 1;
            int offs = index >>> BIT_SHIFT;
            int bit_offs = index & BIT_MASK;
            if ((data[offs] & bit_masks[bit_offs]) != 0) {
                count++;
                if (print)
                    out.println(n);
            }
        }
        return count;
    }

    @Override
    public String getName() {
        return "16 num/byte, int access";
    }
}
