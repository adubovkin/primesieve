import static java.lang.System.out;

public class Main {

    public static void main(String[] args) {
        long N = 100000000;
        System.out.println("Prime numbers up to " + N + "\n");

        Sieve[] er = new Sieve[] {new Er1(), new Er2(), new Er3(), new Sundaram()};
        for (int i = 0; i < er.length; i++) {
            long start = System.currentTimeMillis();
            int count = er[i].find(N, false);
            float stop = System.currentTimeMillis() - start;
            out.println(String.format(er[i].getName() + "\t- found: %d within %.3f sec", count, stop / 1000));
        }
    }
}
