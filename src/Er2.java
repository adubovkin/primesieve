import java.util.Arrays;
import static java.lang.System.out;

/**
 * Sieve of Eratosthenes:
 * - byte access
 * - bit field with 16 numbers per byte (even numbers are stripped):
 *      numbers:   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
 *      bit field: 1     1     1     1     1     1     1     1
 */
public class Er2 implements Sieve {
    public static final int BIT_SHIFT = 3;      // bit field shift
    public static final int BIT_MASK = 0x07;    // bit field mask

    private static byte[] bit_masks = new byte[8];
    private static byte[] neg_bit_masks = new byte[8];
    static {
        for (int i = 0; i < bit_masks.length; i++) {
            bit_masks[i] = (byte) (1 << i);
            neg_bit_masks[i] = (byte) ~bit_masks[i];
//            out.println(i + ") " + Integer.toBinaryString(bit_masks[i] & 0xff) + "\t" + Integer.toBinaryString(neg_bit_masks[i] & 0xff));
        }
    }

    @Override
    public int find(long N, boolean print) {
        byte[] data = new byte[(int) ((N >>> (BIT_SHIFT + 1)) + 1)];    // 16 numbers per byte
        Arrays.fill(data, (byte) -1);
        int maxN = (int) Math.sqrt(N);
        for (int n = 3; n <= maxN; n += 2) {
            int index = (n - 1) >>> 1;
            int offs = index >>> BIT_SHIFT;
            int bit_offs = index & BIT_MASK;
            if ((data[offs] & bit_masks[bit_offs]) != 0) {
                int n2 = n << 1;
                for (int k = n2 + n; k <= N; k += n2) {
                    index = (k - 1) >>> 1;
                    offs = index >>> BIT_SHIFT;
                    bit_offs = index & BIT_MASK;
                    data[offs] &= neg_bit_masks[bit_offs];
                }
            }
        }
        if (print)
            out.println("2");
        int count = 1;
        for (int n = 3; n <= N; n += 2) {
            int index = (n - 1) >>> 1;
            int offs = index >>> BIT_SHIFT;
            int bit_offs = index & BIT_MASK;
            if ((data[offs] & bit_masks[bit_offs]) != 0) {
                count++;
                if (print)
                    out.println(n);
            }
        }
        return count;
    }

    @Override
    public String getName() {
        return "16 num/byte,byte access";
    }
}
